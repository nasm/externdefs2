
[bits 64]
[externdef externdefGlobal]
[externdef externdefExtern]

[extern normalExtern]


[global undefinedGlobal]
[global normalGlobal]

local:
    ret

normalGlobal:
    ret

externdefGlobal:
    ret


_start:
    push rbp
    mov rsp, rbp
    call [externdefExtern]
    call [externdefGlobal]
    call [normalExtern]
    leave
    ret
